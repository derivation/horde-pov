Analyse des points de vue dans *La Horde du Contrevent*
=======================================================

*La Horde du Contrevent* d’Alain Damasio est un roman « choral » : la narration
se fait à travers une succession de points de vue attribués à des personnages
différents.

Le passage d’un point de vue à un autre est marqué par des signes
typographiques spécifiques à chaque personnage. Cette particularité permet
d’automatiser l’analyse de l’attribution d’un passage du texte à un point de
vue particulier, et donc de mesurer la place que prennent les différents
personnages dans le roman.

Utilisation
-----------

Afin d’obtenir les graphiques avec la répartition des points de vue, plusieurs
étapes sont nécessaire : analyse du texte, analyse des données et création
des graphiques, embelissement des graphiques.

[`make`](Makefile) permet d’automatiser l’ensemble du processus.

### Analyse du texte

Le compte sur les différents points de vue est fait par [un script
Python](horde-pov) qui analyse les fichiers HTML de chacun des chapitre qui se
trouvent dans l'édition numérique au format EPUB. En fonction de règles ad-hoc
sur la structure et le contenu du HTML, il associe un point de vue à chaque
passage du livre. Pour chaque passage, il calcule le nombre de signes, de mots
et de phrases grâce à la bibliothèque
[textblob](https://textblob.readthedocs.io/en/dev/) et la compréhension du
français ajouté par [textblob-fr](https://github.com/sloria/textblob-fr).

Afin de pouvoir vérifier que tous les passages du livre ont pu être associé au
bon point de vue, un nouvel EPUB est créé dans le processus, où le point de vue
identifié pour chaque passage est explicitement marqué. Cela permet d’affiner
petit à petit les règles de détection et les différents points de vue qui
composent le livre.

Le script Python génère sur sa sortie un fichier au format CSV contenant une
ligne par partie identifiée, avec le chapitre dans lequel elle se trouve, le
point de vue, le nombre de signes, le nombre de mots, et le nombre de phrases.

Utilisation :

    ./horde-pov [-h] [-v] orig_epub_path marked_epub_path > horde.csv

Note : le [résultat de cette analyse](horde.csv) se trouve dans le dépôt Git
afin de permettre la création automatisée des graphiques sans avoir besoin
de reparcourir le livre.

### Analyse des données et création des graphiques

Le CSV avec les différents points de vue est analysé grâce à
[un script R](graphs.R), en utilisant la bibliothèque de
création de graphiques [ggplot2](https://ggplot2.tidyverse.org/).

Utilisation :

    R --no-save -f graphs.R

### Embelissement des graphiques

Les fichiers SVG des graphiques sont ensuite remaniés à travers [une feuille
XSLT](add-patterns.xslt) pour améliorer leur lisibilité.

Les différentes éléments avec des couleurs définies dans l’étape précédente
pour chaque personnage sont rassemblés au sein d’un groupe avec une unique
classe CSS. Cela permet ensuite d’ajouter des motifs pour améliorer la
lisibilté et permettre une impression en niveau de gris.

Exemple d’utilisation :

    saxonb-xslt -xsl:add-patterns.xslt cartographie.svg filename=cartographie.svg > cartographie-new.svg

Licence
-------

© 2022 Mélissa & Lunar de La dérivation sous [licence WTFPL-2](LICENSE)

`marx.min.css` sous licence [MIT](https://github.com/mblode/marx/blob/main/LICENSE.md) © Matthew Blode
