# “La Horde du Contrevent” character analysis
# © 2019-2021 Lunar
# Licensed under WTFPL-2
#
# Dependencies:
# r-base libsaxonb-java moreutils inkscape optipng zip python3

HORDE_EPUB ?= horde.epub

#DEBUG_OPTS = -v

GRAPHS = \
	repartition-generale.svg \
	repartition-relative-par-chapitre.svg \
	repartition-absolue-par-chapitre.svg \
	cartographie.svg \
	cartographie-01.svg \
	cartographie-02.svg \
	cartographie-03.svg \
	cartographie-04.svg \
	cartographie-05.svg \
	cartographie-06.svg \
	cartographie-07.svg \
	cartographie-08.svg \
	cartographie-09.svg \
	cartographie-10.svg \
	cartographie-11.svg \
	cartographie-12.svg \
	cartographie-13.svg \
	cartographie-14.svg \
	cartographie-15.svg \
	cartographie-16.svg \
	cartographie-17.svg \
	cartographie-18.svg \
	cartographie-19.svg \
	dominante-de-genre-par-chapitre.svg \
	repartition-des-genres-par-chapitre.svg

GRAPHS_PNG = \
	$(patsubst %.svg,%.png,$(GRAPHS)) \
	$(patsubst %.svg,%-nb.png,$(GRAPHS))

SOURCES = \
	  LICENSE \
	  Makefile \
	  horde-pov \
	  graphs.R \
	  add-patterns.xslt

all: $(GRAPHS) $(GRAPHS_PNG) horde-pov.zip

marked.epub horde.csv: horde-pov
	./horde-pov $(DEBUG_OPTS) "$(HORDE_EPUB)" marked.epub > horde.csv || { rm -f horde.csv; exit 1; }

$(GRAPHS): graphs.R horde.csv add-patterns.xslt
	LC_ALL=fr_FR.UTF-8 R --no-save -f $<
	for graph in $(GRAPHS); do \
		echo "Fixing patterns on $$graph"; \
		saxonb-xslt -xsl:add-patterns.xslt $$graph filename=$$graph | sponge $$graph; \
	done

%.png: %.svg
	sed -e '/@media (color)/d' "$<" | inkscape --export-background='white' --export-area-page --export-filename='$@' --pipe
	optipng "$@"

%-nb.png: %.svg
	sed -e 's/class="couleur"//' "$<" | inkscape --export-background='white' --export-area-page --export-filename='$@' --pipe
	optipng '$@'

horde-pov.zip: $(SOURCES)
	rm -f horde-pov.zip
	zip horde-pov.zip $^ 

web: web-stamp
web-stamp: public.md marx.min.css horde-pov.zip $(GRAPHS) $(GRAPHS_PNG)
	mkdir -p public
	cp marx.min.css public/
	cp horde-pov.zip public/
	cp $(GRAPHS) $(GRAPHS_PNG) public/
	pandoc -f markdown -t html5 --standalone -o public/index.html public.md 
	touch web-stamp

clean:
	rm -f marked.epub horde.csv
	rm -f $(GRAPHS) $(GRAPHS_PNG)
	rm -f web-stamp
