---
title: Analyse des points de vue dans La Horde du Contrevent
author: La Dérivation
css:
 - marx.min.css
header-includes:  |
  <style>
    #title-block-header {
      text-align: center;
      margin-top: 0;
      padding-top: 1em;
      padding-bottom: 1em;
      background-color: #ddd;
    }
    figcaption {
      text-align: center;
      font-weight: bold;
      font-family: var(--sans-serif);
    }
    figure .download {
      text-align: center;
    }
  </style>
---

<main>
  <article>

*La Horde du Contrevent* d’Alain Damasio est un roman « choral » : la narration
se fait à travers une succession de points de vue attribués à des personnages
différents.

Le passage d’un point de vue à un autre est marqué par des signes
typographiques spécifiques à chaque personnage. Cette particularité permet
d’automatiser l’analyse de l’attribution d’un passage du texte à un point de
vue particulier, et donc de mesurer la place que prennent les différents
personnages dans le roman.

Les dialogues sont associés au dernier personnage dont on a connaissance du
point de vue.

## Cartographie générale

<figure>
  <a href="cartographie.svg"><img src="cartographie.svg" alt=""></a>
  <figcaption>Cartographie des points de vue du roman</figcaption>
  <div class="download">[SVG interactif](cartographie.svg) | [PNG](cartographie.png) | [PNG N&B](cartographie-nb.png)</div>
</figure>

Pour rappel, voici les différents personnages et leur signe :

Signe        Description
-----------  -------------------------------------
Ω            Golgoth, traceur
π            Pietro Della Rocca, prince
)            Sov Strochnis, scribe
¿’           Caracole, troubadour
Δ            Erg Machaon, combattant-protecteur
¬            Talweg Arcippé, géomaître
>            Firost de Toroge, pilier
^            L’autoursier, oiselier-chasseur
‘,           Steppe Phorehys, fleuron
)-           Arval Redhamaj, éclaireur
ˇ•           Le fauconnier, oiselier-chasseur
∞            Horst et Karst Dubka, ailiers
x            Oroshi Melicerte, aéromaître
(·)          Alme Capys, soigneuse
‹›           Aoi Nan, cueilleuse et sourcière
∫            Larco Scarsa, braconnier du ciel
◊            Léarch, artisan du métal
~            Callirhoé Déicoon, feuleuse
∂            Boscavo Silamphre, artisan du bois
≈            Coriolis, croc
√            Sveziest, croc
]]           Barbak, croc
]            Sélème, stylite

## Répartition sur l’intégralité du livre

<figure>
  <a href="repartition-generale.svg"><img src="repartition-generale.svg" alt=""></a>
  <figcaption>Répartition des points de vue sur l’ensemble du roman</figcaption>
  <div class="download">[SVG](repartition-generale.svg) | [PNG](repartition-generale.png) | [PNG N&B](repartition-generale-nb.png)</div>
</figure>

## Répartition par chapitre

<figure>
  <a href="repartition-relative-par-chapitre.svg"><img src="repartition-relative-par-chapitre.svg" alt=""></a>
  <figcaption>Répartition relative des points de vue par chapitre</figcaption>
  <div class="download">[SVG](repartition-relative-par-chapitre.svg) | [PNG](repartition-relative-par-chapitre.png) | [PNG N&B](repartition-relative-par-chapitre-nb.png)</div>
</figure>

<figure>
  <a href="repartition-absolue-par-chapitre.svg"><img src="repartition-absolue-par-chapitre.svg" alt=""></a>
  <figcaption>Répartition en nombre de signes des points de vue par chapitre</figcaption>
  <div class="download">[SVG](repartition-absolue-par-chapitre.svg) | [PNG](repartition-absolue-par-chapitre.png) | [PNG N&B](repartition-absolue-par-chapitre-nb.png)</div>
</figure>

## Cartographie par chapitre

<figure>
  <a href="cartographie-01.svg"><img src="cartographie-01.svg" alt=""></a>
  <figcaption>XIX Pharéole (Chapitre 1)</figcaption>
  <div class="download">[SVG](cartographie-01.svg) | [PNG](cartographie-01.png) | [PNG N&B](cartographie-01-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-02.svg"><img src="cartographie-02.svg" alt=""></a>
  <figcaption>XVIII Chrones (Chapitre 2)</figcaption>
  <div class="download">[SVG](cartographie-02.svg) | [PNG](cartographie-02.png) | [PNG N&B](cartographie-02-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-03.svg"><img src="cartographie-03.svg" alt=""></a>
  <figcaption>XVII Le cosmos est mon campement (Chapitre 3)</figcaption>
  <div class="download">[SVG](cartographie-03.svg) | [PNG](cartographie-03.png) | [PNG N&B](cartographie-03-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-04.svg"><img src="cartographie-04.svg" alt=""></a>
  <figcaption>XVI Une certaine qualité d’os (Chapitre 4)</figcaption>
  <div class="download">[SVG](cartographie-04.svg) | [PNG](cartographie-04.png) | [PNG N&B](cartographie-04-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-05.svg"><img src="cartographie-05.svg" alt=""></a>
  <figcaption>XV L’Escadre frêle (Chapitre 5)</figcaption>
  <div class="download">[SVG](cartographie-05.svg) | [PNG](cartographie-05.png) | [PNG N&B](cartographie-05-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-06.svg"><img src="cartographie-06.svg" alt=""></a>
  <figcaption>XIV Si tu veux, je l’ai touché… (Chapitre 6)</figcaption>
  <div class="download">[SVG](cartographie-06.svg) | [PNG](cartographie-06.png) | [PNG N&B](cartographie-06-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-07.svg"><img src="cartographie-07.svg" alt=""></a>
  <figcaption>XIII La dernière Horde ? (Chapitre 7)</figcaption>
  <div class="download">[SVG](cartographie-07.svg) | [PNG](cartographie-07.png) | [PNG N&B](cartographie-07-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-08.svg"><img src="cartographie-08.svg" alt=""></a>
  <figcaption>XII Le Corroyeur (Chapitre 8)</figcaption>
  <div class="download">[SVG](cartographie-08.svg) | [PNG](cartographie-08.png) | [PNG N&B](cartographie-08-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-09.svg"><img src="cartographie-09.svg" alt=""></a>
  <figcaption>XI La tour Fontaine (Chapitre 9)</figcaption>
  <div class="download">[SVG](cartographie-09.svg) | [PNG](cartographie-09.png) | [PNG N&B](cartographie-09-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-10.svg"><img src="cartographie-10.svg" alt=""></a>
  <figcaption>X Le siphon (Chapitre 10)</figcaption>
  <div class="download">[SVG](cartographie-10.svg) | [PNG](cartographie-10.png) | [PNG N&B](cartographie-10-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-11.svg"><img src="cartographie-11.svg" alt=""></a>
  <figcaption>IX L’Outre et le Lorsque (Chapitre 11)</figcaption>
  <div class="download">[SVG](cartographie-11.svg) | [PNG](cartographie-11.png) | [PNG N&B](cartographie-11-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-12.svg"><img src="cartographie-12.svg" alt=""></a>
  <figcaption>VIII Alticcio (Chapitre 12)</figcaption>
  <div class="download">[SVG](cartographie-12.svg) | [PNG](cartographie-12.png) | [PNG N&B](cartographie-12-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-13.svg"><img src="cartographie-13.svg" alt=""></a>
  <figcaption>VII La tour d’Ær (Chapitre 13)</figcaption>
  <div class="download">[SVG](cartographie-13.svg) | [PNG](cartographie-13.png) | [PNG N&B](cartographie-13-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-14.svg"><img src="cartographie-14.svg" alt=""></a>
  <figcaption>VI Véramorphe (Chapitre 14)</figcaption>
  <div class="download">[SVG](cartographie-14.svg) | [PNG](cartographie-14.png) | [PNG N&B](cartographie-14-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-15.svg"><img src="cartographie-15.svg" alt=""></a>
  <figcaption>V Ceux qui nous ont enfantés (Chapitre 15)</figcaption>
  <div class="download">[SVG](cartographie-15.svg) | [PNG](cartographie-15.png) | [PNG N&B](cartographie-15-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-16.svg"><img src="cartographie-16.svg" alt=""></a>
  <figcaption>IV Norska, à travers l’échancrure (Chapitre 16)</figcaption>
  <div class="download">[SVG](cartographie-16.svg) | [PNG](cartographie-16.png) | [PNG N&B](cartographie-16-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-17.svg"><img src="cartographie-17.svg" alt=""></a>
  <figcaption>III Krafla (Chapitre 17)</figcaption>
  <div class="download">[SVG](cartographie-17.svg) | [PNG](cartographie-17.png) | [PNG N&B](cartographie-17-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-18.svg"><img src="cartographie-18.svg" alt=""></a>
  <figcaption>II Le vif (Chapitre 18)</figcaption>
  <div class="download">[SVG](cartographie-18.svg) | [PNG](cartographie-18.png) | [PNG N&B](cartographie-18-nb.png)</div>
</figure>

<figure>
  <a href="cartographie-19.svg"><img src="cartographie-19.svg" alt=""></a>
  <figcaption>I La neuvième forme (Chapitre 19)</figcaption>
  <div class="download">[SVG](cartographie-19.svg) | [PNG](cartographie-19.png) | [PNG N&B](cartographie-19-nb.png)</div>
</figure>

## Répartition par genre des personnages

<figure>
  <a href="repartition-des-genres-par-chapitre.svg"><img src="repartition-des-genres-par-chapitre.svg" alt=""></a>
  <figcaption>Répartition des signes par genre des personnages et par chapitre</figcaption>
  <div class="download">[SVG](repartition-des-genres-par-chapitre.svg) | [PNG](repartition-des-genres-par-chapitre.png) | [PNG N&B](repartition-des-genres-par-chapitre-nb.png)</div>
</figure>

<figure>
  <a href="dominante-de-genre-par-chapitre.svg"><img src="dominante-de-genre-par-chapitre.svg" alt=""></a>
  <figcaption>Dominante de signes par genre des personnages et par chapitre</figcaption>
  <div class="download">[SVG](dominante-de-genre-par-chapitre.svg) | [PNG](dominante-de-genre-par-chapitre.png) | [PNG N&B](dominante-de-genre-par-chapitre-nb.png)</div>
</figure>

## Méthodologie

Les calculs ont été fait par un script Python qui analyse les fichiers HTML de
chacun des chapitre qui se trouvent dans l'édition numérique au format EPUB. En
fonction de règles ad-hoc sur la structure et le contenu du HTML, il
associe un point de vue à chaque passage du livre. Pour chaque passage, il
calcule le nombre de signes, de mots et de phrases grâce à la bibliothèque
[textblob](https://textblob.readthedocs.io/en/dev/) et la compréhension du
français ajouté par [textblob-fr](https://github.com/sloria/textblob-fr).

Afin de pouvoir vérifier que tous les passages du livre ont pu être associé au
bon point de vue, un nouvel EPUB est créé dans le processus, où le point de vue
identifié pour chaque passage est explicitement marqué. Cela a permis d’affiner
petit à petit les règles de détection et les différents points de vue qui
composent le livre.

Le script Python génère un fichier au format CSV contenant une ligne par partie
identifiée, avec le chapitre dans lequel elle se trouve, le point de vue, le
nombre de signes, le nombre de mots, et le nombre de phrases.

Ces données sont ensuite analysées grâce à [R](https://www.r-project.org/), et
sa bibliothèque de création de graphiques [ggplot2](https://ggplot2.tidyverse.org/).

Les fichiers SVG des graphiques sont ensuite remaniés à travers une feuille
XSLT pour améliorer leur lisibilité.

 * [Code source](horde-pov.zip) (archive ZIP ; make, Python, R, XSLT ; sous licence WTFPL-2)
 * SHA256 du fichier EPUB utilisé en entrée : `97fc5d58f75c25e615ff8aa03cb66b329b32486c95b542a34c0818c1cf623975`

  </article>

</main>

<footer>

© 2022 Mélissa & Lunar de La dérivation sous licence [Creative Commons BY-SA-NC 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)\
(pour toute utilisation commerciale, veuillez nous contacter). 

</footer>
